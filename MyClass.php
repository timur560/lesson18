<?php
/**
 * Created by PhpStorm.
 * User: oculus
 * Date: 26.06.19
 * Time: 19:43
 */

class MyClass
{
    public $name;
    public $age;

    /**
     * @return string
     */
    public function getName(): string
    {
        $result = $this->name;
        return $result;
    }

    /**
     * @param string $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }


}
